# @rdub/next-markdown
Markdown utilities for [Next.js]

<a href="https://npmjs.org/package/@rdub/next-markdown" title="View @rdub/next-markdown on NPM"><img src="https://img.shields.io/npm/v/@rdub/next-markdown.svg" alt="@rdub/next-markdown NPM version" /></a>

```jsx
import Markdown, { MD } from '@rdub/next-markdown/md'

export default function Page() {
    return <div>
        <Markdown className={"foo"} content={`
            # Hello, world!
            Here is some [GitHub-flavored Markdown][GFMD],
            rendered with the \`Markdown\` component.

            ## Margins are stripped, by default
            Normally, 4 leading spaces denote a code block
            (similar to a triple-backtick line).

            [GFMD]: https://github.github.com/gfm/
        `}/>
        {MD(`
            ### \`MD\` function
            You can also pass a string directly to \`MD\`:

                MD("## Markdown!")
        `)}
    </div>
}
```

## Example usage
- [ctbk.dev](https://github.com/neighbor-ryan/ctbk.dev/blob/c2ccbcd4e2f64be3e2a6919b1c50ee6cbd8f9a5e/www/pages/index.tsx#L660-L667)

[Next.js]: https://nextjs.org/
