import remarkGfm from "remark-gfm";
import React, {DetailedHTMLProps, HTMLAttributes} from "react";
import Link from "next/link";
import ReactMarkdown, { Components } from "react-markdown";
import rehypeRaw from 'rehype-raw'
const {isArray} = Array;

export function sanitizeElement(elem: any): string {
    if (typeof elem === 'string') {
        return elem.toLowerCase().replaceAll(/\W/g, "-")
    } else if (elem?.value) {
        return sanitizeElement(elem.value)
    } else if (elem?.children) {
        return sanitizeElement(elem.children)
    } else {
        return elem.map(sanitizeElement).join("")
    }
}

export const ID_RGX = ` id=([a-zA-Z0-9-']+)$`

export function extractId(s: string) {
    const match = s.match(ID_RGX)
    return match ? { id: match[1], title: s.substring(0, match.index) } : { title: s }
}

type Props = DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>

export const renderHeading = (
    Tag: "h1" | "h2" | "h3" | "h4" | "h5" | "h6",
    props: Props
) => {
    const { children, id, ...rest } = props
    if (id) {
        return <Tag {...props} />
    }
    // console.log("heading:", children, isArray(children))
    if (typeof children === 'string') {
        const { id, title, } = extractId(children)
        return id ? <Tag id={id} {...rest}><a href={`#${id}`}>{title}</a></Tag> : <Tag {...props} />
    } else if (isArray(children)){
        const prefix = children.slice(0, children.length - 1)
        const last = children[children.length - 1]
        if (typeof last === 'string') {
            const { id, title, } = extractId(last)
            const newChildren = prefix.concat([title])
            return id ? <Tag id={id} {...rest}><a href={`#${id}`}>{newChildren}</a></Tag> : <Tag {...props} />
        }
    } else {
        console.warn("Unrecognized children:", children)
    }
    return <Tag {...props} />
}

export const components: Components = {
    a: ({ href, children }) =>
        <Link href={href || "#"} target={href?.startsWith("http") ? "_blank" : "_self"}>
            {children}
        </Link>,
    img: ({ src, alt, node, ...props}) => {
        //return <Image src={src || ''} {...props} />
        return <img src={src || ''} alt={alt} {...props} />
    },
    h1: ({ node, ...props }) => renderHeading("h1", props),
    h2: ({ node, ...props }) => renderHeading("h2", props),
    h3: ({ node, ...props }) => renderHeading("h3", props),
    h4: ({ node, ...props }) => renderHeading("h4", props),
    h5: ({ node, ...props }) => renderHeading("h5", props),
    h6: ({ node, ...props }) => renderHeading("h6", props),
}

export type Opts = {
    className?: string
    stripMargin?: boolean
}

export function stripMargin(content: string) {
    const lines = content.split("\n")
    const marginSize = lines.map(line => {
        const match = line.match(/^\s*/)
        if (match) {
            const spaces = match[0].length
            return spaces === line.length ? null : spaces
        } else {
            return null
        }
    }).filter((size): size is number => size !== null).reduce((a, b) => Math.min(a, b), Infinity)
    return lines.map(line => line.substring(marginSize)).join("\n")
}

export default function Markdown(
    {
        content,
        className,
        stripMargin: _stripMargin = true,
        components: _components = {},
    }: {
        content: string
        components?: Partial<Components>
    } & Opts) {
    if (_stripMargin) {
        content = stripMargin(content)
    }
    return <ReactMarkdown
        remarkPlugins={[remarkGfm]}
        rehypePlugins={[rehypeRaw]}
        components={{ ...components, ..._components }}
        className={className}
    >
        {content}
    </ReactMarkdown>
}

export function MD(content: string, { className, stripMargin = true }: Opts = {}) {
    return (
        <Markdown
            content={content}
            className={className}
            stripMargin={stripMargin}
        />
    )
}
